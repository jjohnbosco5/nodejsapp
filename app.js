const express = require('express')
const app = express()
const port = 8080

app.get('/', (req, res) => {
  res.send('Sjb Node application for CICD developed by N4si!!')
})

app.listen(port, () => {
  console.log(`Application is listening at http://localhost:${port}`)
})
